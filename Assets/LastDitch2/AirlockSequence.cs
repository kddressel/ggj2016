using UnityEngine;
using System.Collections;

public class AirlockSequence : MonoBehaviour
{
    public bool SuitIsOn;
    public bool FirstDoorIsOpen;
    public bool AirlockIsFilledWithAir;
    public bool SecondDoorOpen;

		AudioClip Outside;

    public void PutOnSuit()
    {
        SuitIsOn = true;
				SoundPlayer.Play("PutHelmetOn");
    }



    public void OpenFirstDoor()
    {
			FirstDoorIsOpen = true;
			SoundPlayer.Play("Door Open");

				if (!AirlockIsFilledWithAir)
				{
						SoundPlayer.Play("Failure");
						EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Oxygen, false);
				}

				if (SecondDoorOpen)
				{
						SoundPlayer.Play("Failure");
						EventRouter.Instance.DeathEvent.Invoke(DeathType.ExplosiveDecompression);
				}
		}



    public void CloseFirstDoor()
    {
        FirstDoorIsOpen = false;
				SoundPlayer.Play("Door Close");
    }



    public void OperateFirstDoor(bool open)
    {
        if (open && !FirstDoorIsOpen)
        {
            OpenFirstDoor();
        }
        else if (!open && FirstDoorIsOpen)
        {
            CloseFirstDoor();
        }
    }





    public void CycleAirOutOfAirlock()
    {
        AirlockIsFilledWithAir = false;
				SoundPlayer.Play("AirEscapeStart");
				SoundPlayer.Play("AirEscapingLoop");

        if (FirstDoorIsOpen)
				{
						SoundPlayer.Play("Failure");
            EventRouter.Instance.DeathEvent.Invoke(DeathType.ExplosiveDecompression);
        }

        if (!SuitIsOn)
				{
						SoundPlayer.Play("Failure");
            EventRouter.Instance.DeathEvent.Invoke(DeathType.Suffocation);
        }
    }



    public void OpenSecondDoor()
    {
			SecondDoorOpen = true;
			SoundPlayer.Play("Door Open");

        if (!SuitIsOn)
				{
						SoundPlayer.Play("Failure");
            EventRouter.Instance.DeathEvent.Invoke(DeathType.Suffocation);
						return;
        }

        if (AirlockIsFilledWithAir)
				{
						SoundPlayer.Play("Failure");
						EventRouter.Instance.DeathEvent.Invoke(DeathType.ExplosiveDecompression);
						return;
        }


        if (FirstDoorIsOpen)
				{
						SoundPlayer.Play("Failure");
						EventRouter.Instance.DeathEvent.Invoke(DeathType.ExplosiveDecompression);
						return;
        }

				SoundPlayer.Play("Success");

				SoundPlayer.GoOutside();
    }




    public void CloseSecondDoor()
    {
			SecondDoorOpen = false;
			SoundPlayer.Play("Door Close");

			if(!SuitIsOn)
			{
				SoundPlayer.GoInside();
			}
    }


    public void OperateSecondDoor(bool open)
    {
        if (open && !SecondDoorOpen)
        {
            OpenSecondDoor();
        }
        else if (!open && SecondDoorOpen)
        {
            CloseSecondDoor();
        }
    }



    public void CycleAirIntoAirlock()
		{
				SoundPlayer.Play("AirEscapeEnd");
        if (SecondDoorOpen)
				{
						SoundPlayer.Play("Failure");
            EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Oxygen, false);
        }
        else
        {
            AirlockIsFilledWithAir = true;
        }
    }



    public void TakeOffSuit()
    {
			SuitIsOn = false;
			SoundPlayer.Play("TakeHelmetOff");
    }


    public void OperateSuit(bool putOn)
    {
        if (putOn && !SuitIsOn)
            PutOnSuit();
        else if (!putOn && SuitIsOn)
            TakeOffSuit();
    }

		public void OperateAirCycle(bool cycleAirIn)
		{
			if (cycleAirIn && !AirlockIsFilledWithAir)
				CycleAirIntoAirlock();
			else if (!cycleAirIn && AirlockIsFilledWithAir)
				CycleAirOutOfAirlock();
		}
}
