﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class DoorVisual : MonoBehaviour
{
    

    public UnityBoolEvent OnToggleDoor;

    public bool DoorIsOpen;
    public float DoorSpeed = 0.2f;
    private float _position;


    private float _originalDoorScale;

    public void OpenDoor()
    {
        DoorIsOpen = true;
        OnToggleDoor.Invoke(true);
    }

    public void CloseDoor()
    {
        DoorIsOpen = false;
        OnToggleDoor.Invoke(false);
    }

    public void ToggleDoor()
    {
        if (DoorIsOpen)
        {
            CloseDoor();
        }
        else
        {
            OpenDoor();
        }
    }


    void Start()
    {
        _originalDoorScale = transform.localScale.y;
    }


    void Update()
    {
        if (DoorIsOpen)
            _position = Mathf.Lerp(_position, 0f, DoorSpeed);
        else
        {
            _position = Mathf.Lerp(_position, _originalDoorScale, DoorSpeed);
        }

        transform.localScale = new Vector3(transform.localScale.x, _position, transform.localScale.z);
    }
}
