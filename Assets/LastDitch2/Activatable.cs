﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Activatable : MonoBehaviour
{
    public UnityEvent OnActivate;
    public string ActivatorTag = "Player";
    public bool ActivatorIsInRange;


    void Update()
    {
        if (ActivatorIsInRange && Input.GetKeyDown(KeyCode.Space))
            OnActivate.Invoke();
    }




    void OnTriggerStay2D(Collider2D otherCollider)
    {

        if (otherCollider.gameObject.tag == ActivatorTag)
        {
            ActivatorIsInRange = true;
        }
    }

    void OnTriggerExit2D(Collider2D otherCollider)
    {
        if (otherCollider.gameObject.tag == ActivatorTag)
            ActivatorIsInRange = false;
    }
}
