using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Controller2D : MonoBehaviour
{
    public LayerMask CollisionMask;

    const float SkinWidth = 0.045f;
    private BoxCollider2D _collider;
    private Rigidbody2D _rigidbody2D;
    private RaycastOrigins _raycastOrigins;

    public int EdgeRayCount = 4;

    private float _horizontalRaySpacing;
    private float _verticalRaySpacing;


    void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        _rigidbody2D = GetComponent<Rigidbody2D>();

        _rigidbody2D.isKinematic = true;

        CalculateRaySpacing();
    }

    void Update()
    {
        _rigidbody2D.WakeUp();
    }



    public void Move(Vector3 velocity)
    {
        UpdateRaycastOrigins();
        Collisions(ref velocity);
        transform.Translate(velocity);
    }



    void Collisions(ref Vector3 velocity)
    {
        float directionY = Mathf.Sign(velocity.y);
        float directionX = Mathf.Sign(velocity.x);

        float rayLengthY = Mathf.Abs(velocity.y) + SkinWidth;
        float rayLengthX = Mathf.Abs(velocity.x) + SkinWidth;

        for (int i = 0; i < EdgeRayCount; i++)
        {
            Vector2 rayOrigin = (directionY == -1) ? _raycastOrigins.BottomLeft : _raycastOrigins.TopLeft;
            rayOrigin += Vector2.right*(_verticalRaySpacing*i + velocity.x);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up*directionY, rayLengthY, CollisionMask);

            if (hit)
            {
                velocity.y = (hit.distance - SkinWidth)*directionY;
                rayLengthY = hit.distance;
            }
        }

        for (int i = 0; i < EdgeRayCount; i++)
        {
            Vector2 rayOrigin = (directionX == -1) ? _raycastOrigins.BottomLeft : _raycastOrigins.BottomRight;
            rayOrigin += Vector2.up * (_horizontalRaySpacing * i + velocity.y);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLengthX, CollisionMask);

            if (hit)
            {
                velocity.x = (hit.distance - SkinWidth) * directionX;
                rayLengthX = hit.distance;
            }
        }
    }




    void UpdateRaycastOrigins()
    {
        Bounds bounds = _collider.bounds;
        bounds.Expand(SkinWidth * -2);

        _raycastOrigins.BottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        _raycastOrigins.BottomRight = new Vector2(bounds.max.x, bounds.min.y);
        _raycastOrigins.TopLeft = new Vector2(bounds.min.x, bounds.max.y);
        _raycastOrigins.TopRight = new Vector2(bounds.max.x, bounds.max.y);

    }



    void CalculateRaySpacing()
    {
        Bounds bounds = _collider.bounds;
        bounds.Expand(SkinWidth * -2);

        EdgeRayCount = Mathf.Clamp(EdgeRayCount, 2, int.MaxValue);
        EdgeRayCount = Mathf.Clamp(EdgeRayCount, 2, int.MaxValue);

        _horizontalRaySpacing = bounds.size.y/(EdgeRayCount - 1);
        _verticalRaySpacing = bounds.size.x/(EdgeRayCount - 1);
    }




    struct RaycastOrigins
    {
        public Vector2 TopRight, 
            BottomRight, 
            BottomLeft, 
            TopLeft;
    }
}
