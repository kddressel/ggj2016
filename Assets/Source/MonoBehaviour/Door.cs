using UnityEngine;

/// <summary>
/// An example of an interactable that itself changes states independently of anything outside of it.
/// </summary>
public class Door : MonoBehaviour, IInteractable
{
	public Sprite OpenSprite;
	public Sprite ClosedSprite;

	private bool IsOpen;

	SpriteRenderer Renderer;
	Collider2D Collider;

	void Awake()
	{
		Collider = GetComponent<Collider2D>();
		Renderer = GetComponentInChildren<SpriteRenderer>();
	}

	public void Interact()
	{
		SetState(!IsOpen);
	}

	public void SetState(bool enabled)
	{
		IsOpen = enabled;
		Renderer.sprite = enabled ? OpenSprite : ClosedSprite;
		Collider.enabled = !enabled;
	}
}