﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class PlayerVisuals : MonoBehaviour
{

    private SpriteRenderer _spriteRenderer;
    public Sprite SpaceSuitOffSprite;
    public Sprite SPaceSuitOnSprite;
    public Sprite DED;

    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }


    public void DonSpacesuit()
    {
        _spriteRenderer.sprite = SPaceSuitOnSprite;
    }

    public void TakeOffSpacesuit()
    {
        _spriteRenderer.sprite = SpaceSuitOffSprite;
    }

    public void OperateSpacesuit(bool placeOnRack)
    {
        if (placeOnRack)
        {
            DonSpacesuit();
        }
        else
        {
            TakeOffSpacesuit();
        }
    }

    public void Ded()
    {
        _spriteRenderer.sprite = DED;
    }

}
