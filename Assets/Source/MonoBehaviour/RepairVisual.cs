using UnityEngine;
using System.Collections;

public class RepairVisual : MonoBehaviour
{
		public ResourceType Resource;
    private SpriteRenderer _spriteRenderer;
		private ParticleSystem _particleSystem;
    public bool IsBroken;

    public UnityBoolEvent OnWorkingEvent;


	// Use this for initialization
	void Awake ()
	{
	    _spriteRenderer = GetComponent<SpriteRenderer>();
			_particleSystem = GetComponentInChildren<ParticleSystem>();
	}




    public void Repair()
    {
			if(IsBroken)
			{
				IsBroken = false;
				OnWorkingEvent.Invoke(true);

				if (_spriteRenderer != null)
				{
					_spriteRenderer.enabled = false;
				}

				if(_particleSystem != null)
				{
					var emission = _particleSystem.emission;
					emission.enabled = false;
				}
			}
    }


    public void Break(ResourceType resource)
    {
			if (resource == Resource)
			{
				IsBroken = true;
				OnWorkingEvent.Invoke(false);

				if(_spriteRenderer != null)
				{
					_spriteRenderer.enabled = true;
				}

				if (_particleSystem != null)
				{
					var emission = _particleSystem.emission;
					emission.enabled = true;
				}
			}
    }
}
