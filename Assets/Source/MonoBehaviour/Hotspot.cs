using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Hotspot : MonoBehaviour
{
	public bool IsPlayerInRange { get; private set; }

	private void OnTriggerEnter2D(Collider2D c)
	{
		IsPlayerInRange = true;
	}

	private void OnTriggerExit2D(Collider2D c)
	{
		IsPlayerInRange = false;
	}
}
