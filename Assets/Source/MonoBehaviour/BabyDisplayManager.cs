using UnityEngine;
using System.Collections;

public class BabyDisplayManager : MonoBehaviour {

	public Sprite LiveBaby;
	public Sprite DeadBaby;

	SpriteRenderer[] Renderers;
	// Use this for initialization
	void Start () {
		Renderers = GetComponentsInChildren<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		var baseSystems = GameObject.FindObjectOfType<BaseSystems>();

		// 3 cribs are hardcoded on the map for now, so can't use the max
		for(int i = 0; i < 3; i++)
		{
			Renderers[i].sprite = baseSystems.Babies > i ? LiveBaby : DeadBaby;
		}
	}
}
