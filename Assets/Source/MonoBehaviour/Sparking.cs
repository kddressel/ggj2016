using UnityEngine;
using System.Collections;

/// <summary>
/// A low level interactable which sparks when it needs attention
/// </summary>
public class Sparking : MonoBehaviour, IInteractable
{
	ParticleSystem ParticleSystem;

	void Awake()
	{
		ParticleSystem = GetComponentInChildren<ParticleSystem>();
	}

	public void Interact()
	{
		SetState(!ParticleSystem.emission.enabled);
	}

	public void SetState(bool enabled)
	{
		var emission = ParticleSystem.emission;
		emission.enabled = enabled;
	}
}
