using UnityEngine;
using UnityEngine.Events;

public class PlayerInputHandler : MonoBehaviour
{
	public float MoveSpeed = 5;
    public Vector2 DesiredMotionVector;

	Controller2D _controller2D;
	
	void Start ()
	{
	    _controller2D = FindObjectOfType<Controller2D>();
	}

	void Update ()
	{
		DesiredMotionVector = new Vector2 (Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }


    void FixedUpdate()
    {
		_controller2D.Move (Vector2.ClampMagnitude(DesiredMotionVector, 1f) * MoveSpeed * Time.fixedDeltaTime);
    }
}