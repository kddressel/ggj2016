using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDElement : MonoBehaviour
{
	public Text ValueText;

	public void SetValue(string val)
	{
		ValueText.text = val;
	}
}
