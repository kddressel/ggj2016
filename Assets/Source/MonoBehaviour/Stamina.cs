using UnityEngine;
using System.Collections;

public class Stamina : MonoBehaviour
{
    public int BonusAdded = 3;
    public int DefaultAdded = 1;
    public int Value;
    public bool HasEatenToday = false;


    private static Stamina _Instance;
    public static Stamina Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = GameObject.FindObjectOfType<Stamina>();
            }
            return _Instance;
        }
    }

    public bool CheckAndSubtractStamina()
    {
        var success = Value > 0;
        if (success)
        {
            Value--;
        }
        else
				{
						SoundPlayer.Play("Failure");
            Debug.Log("Not enough stamina");
        }
        return success;
    }

    public void EatFood()
    {
        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				HasEatenToday = true;
				SoundPlayer.Play("EatFood");
    }

    public void OnNextDay()
    {
        if (HasEatenToday)
        {
            Value += BonusAdded;
        }
        else
        {
            Value += DefaultAdded;
        }

        // reset for next day
        HasEatenToday = false;
    }
}
