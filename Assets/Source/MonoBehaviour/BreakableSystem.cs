using UnityEngine;
using System;

public class BreakableSystem : MonoBehaviour
{
	public int MaxLevel;
	
	public bool BrokenToday { get; private set; }
	public bool FixedToday { get; private set; }
	public int Level { get; private set; }


	void Awake()
	{
		Level = MaxLevel;
		ResetRepairs();
	}

	public void ResetRepairs()
	{
		BrokenToday = false;
		FixedToday = false;
	}
}