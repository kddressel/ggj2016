/// <summary>
/// An interactable has a hotspot and a response to user interaction
/// Any part which is interactable (can be used by pressing space in its hotspot) must implement this interface to define what it does upon interaction.
/// </summary>
public interface IInteractable
{
	/// <summary>
	/// Respond to user interaction
	/// </summary>
	void Interact();

	/// <summary>
	/// Set an explicit state for the interactable. Useful for starting a new day in a specific state.
	/// </summary>
	/// <param name="enabled"></param>
	void SetState(bool enabled);
}