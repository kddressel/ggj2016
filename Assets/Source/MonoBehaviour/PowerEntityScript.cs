﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class PowerEntityScript : MonoBehaviour {

    [Serializable]
    public class PowerEntityEvent : UnityEvent
    {
    }



    public PowerEntityEvent PowerGainEvent;



    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PowerGainEvent.Invoke();
        }
    }
}
