using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour
{
	static SoundPlayer Instance { get; set; }

	public AudioSource AtmosphereLoop;

	void Awake()
	{
		Instance = this;
	}

	public static AudioSource Play(string sound)
	{
		var source = Instance.gameObject.AddComponent<AudioSource>();
		source.PlayOneShot(Resources.Load<AudioClip>("Sounds/" + sound));
		return source;
	}

	public static void GoOutside()
	{
		Instance.AtmosphereLoop.clip = Resources.Load<AudioClip>("Sounds/OutsideAtmosphereLoop");
		Instance.AtmosphereLoop.Play();
	}

	public static void GoInside()
	{
		Instance.AtmosphereLoop.clip = Resources.Load<AudioClip>("Sounds/BaseAtmosphereLoop");
		Instance.AtmosphereLoop.Play();
	}

	void Update()
	{
		// cleanup old one shot sounds
		foreach(var source in GetComponents<AudioSource>())
		{
			if(!source.isPlaying && source.clip == null)
			{
				Destroy(source);
			}
		}
	}
}
