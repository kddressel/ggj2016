using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class SpacesuitVisual : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    public bool SpacesuitIsInUse;

    public UnityBoolEvent OnSpacesuitEvent;


	// Use this for initialization
	void Start ()
	{
	    _spriteRenderer = GetComponent<SpriteRenderer>();
	}




    public void TakeSpacesuit()
    {
        SpacesuitIsInUse = true;
        OnSpacesuitEvent.Invoke(true);
        _spriteRenderer.enabled = false;
    }


    public void ReplaceSpacesuit()
    {
        SpacesuitIsInUse = false;
        OnSpacesuitEvent.Invoke(false);
        _spriteRenderer.enabled = true;
    }


    public void ToggleSpacesuit()
    {
        if (SpacesuitIsInUse)
            ReplaceSpacesuit();
        else
        {
            TakeSpacesuit();
        }
    }
}
