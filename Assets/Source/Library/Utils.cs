using UnityEngine;

public static class Utils
{
	/// <summary>
	/// Quick accessor to get the position of the transform as a Vector2.
	/// C# doesn't have extension properties, so had to make this a function
	/// </summary>
	public static Vector2 position2d(this Transform transform)
	{
		return new Vector2(transform.position.x, transform.position.y);
	}
}