using UnityEngine;

public class SolarPanelSequence : MonoBehaviour
{
    public bool WingsAreWorking;
    public bool CapacitorIsWorking;
    public bool CordIsWorking;


		public void ToggleWingsWorking(bool isWorking)
		{
			if(!isWorking)
			{
				// support breaking things from BaseSystems day script
				WingsAreWorking = false;
			}
			else
			{
				RepairTheWings();
			}
		}

		public void ToggleCapacitorWorking(bool isWorking)
		{
			if (!isWorking)
			{
				// support breaking things from BaseSystems day script
				CapacitorIsWorking = false;
			}
			else
			{
				ResetTheCapacitor();
			}
		}

		public void ToggleWireWorking(bool isWorking)
		{
			if (!isWorking)
			{
				// support breaking things from BaseSystems day script
				CordIsWorking = false;
			}
			else
			{
				MendTheCord();
			}
		}

    public void RepairTheWings()
		{
				// don't repair if already repaired
				if (WingsAreWorking) return;

        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				WingsAreWorking = true;
				SoundPlayer.Play("FixSolarPanel");

        if (CordIsWorking && CapacitorIsWorking)
				{
						SoundPlayer.Play("Success");
            EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Power, true);
        }
        else if (!CordIsWorking && CapacitorIsWorking)
        {
						// TODO: Capacitor Explodes
						SoundPlayer.Play("Failure");
            EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Power, false);
            CapacitorIsWorking = false;
        }
    }


    public void ResetTheCapacitor()
		{
				// don't repair if already repaired
				if (CapacitorIsWorking) return;

        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				CapacitorIsWorking = true;
				SoundPlayer.Play("FixCapacitor");

        if (WingsAreWorking && !CordIsWorking)
        {
						// TODO: Capacitor Explodes
						SoundPlayer.Play("Failure");
            EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Power, false);
            CapacitorIsWorking = false;
        }
        else if (WingsAreWorking && CordIsWorking)
				{
						SoundPlayer.Play("Success");
            EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Power, true);
        }
    }


    public void MendTheCord()
		{
				// don't repair if already repaired
				if (CordIsWorking) return;

        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				CordIsWorking = true;
				SoundPlayer.Play("FixWire");

        if (WingsAreWorking && CapacitorIsWorking)
				{
						SoundPlayer.Play("Failure");
            EventRouter.Instance.DeathEvent.Invoke(DeathType.Electrocution);
        }
    }




}