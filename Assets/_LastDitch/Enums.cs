using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum AlertLevel { Warning, Critical }

public enum ResourceType { Power, Oxygen, Babies, Signal }

public enum DeathType { Electrocution, Suffocation, ExplosiveDecompression, OutOfResource }

public enum FailureType { AllBabiesDead, UserDeath, CompleteSignalLoss }