using UnityEngine;

public class RadarSequence : MonoBehaviour
{
    public bool SignalAmplifierIsActive;
    public bool DishIsAligned;
    public bool ConsoleIsDebugged;



		public void ToggleSignalAmplifierWorking(bool isWorking)
		{
			if (!isWorking)
			{
				// support breaking things from BaseSystems day script
				SignalAmplifierIsActive = false;
			}
			else
			{
				RechargeSignalAmplifier();
			}
		}

		public void ToggleDishWorking(bool isWorking)
		{
			if (!isWorking)
			{
				// support breaking things from BaseSystems day script
				DishIsAligned = false;
			}
			else
			{
				AlignTheDish();
			}
		}

		public void ToggleConsoleWorking(bool isWorking)
		{
			if (!isWorking)
			{
				// support breaking things from BaseSystems day script
				ConsoleIsDebugged = false;
			}
			else
			{
				DebugTheConsole();
			}
		}


    public void RechargeSignalAmplifier()
    {
        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				SignalAmplifierIsActive = true;
				SoundPlayer.Play("AmplifySignal");

        if (DishIsAligned && ConsoleIsDebugged)
				{
						SoundPlayer.Play("Success");
            EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Signal, true);
            return;
        }

				SoundPlayer.Play("Failure");
        EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Signal, false);
        SignalAmplifierIsActive = false;
    }



    public void AlignTheDish()
    {
        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				DishIsAligned = true;
				SoundPlayer.Play("Recalibrate");

        if (!ConsoleIsDebugged)
				{
						SoundPlayer.Play("Failure");
            DishIsAligned = false;
        }
    }



    public void DebugTheConsole()
    {
        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				SoundPlayer.Play("DebugComputer");
        ConsoleIsDebugged = true;
    }
}