using UnityEngine;
using System;

public class BreakableSystem2 : MonoBehaviour
{
	public ResourceType ResourceId;
	public int MaxLevel;
	public bool KillsBabies;

	public bool BrokenToday { get; private set; }


	void Awake()
	{
		ResetRepairs();
	}

	public void ResetRepairs()
	{
		BrokenToday = false;
	}

	public void Fix(ResourceType resource, bool isNotPenalty)
	{
		if (resource == ResourceId && isNotPenalty)
		{
			BrokenToday = false;
		}
	}

	public void Break(ResourceType resource)
	{
		if (resource == ResourceId)
		{
			BrokenToday = true;
		}
	}

	
}