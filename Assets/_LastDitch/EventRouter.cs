
using System;
using UnityEngine;
using UnityEngine.Events;

public class EventRouter : MonoBehaviour
{
    // GLOBAL EVENTS //
    public ResourceUnityEvent ResourceEvent;
    public DeathUnityEvent DeathEvent;
    public BreakSystemUnityEvent BreakSystemEvent;
    public GameOverUnityEvent GameOverEvent;
    public UnityBoolEvent RescueStalledEvent;

    private static EventRouter _Instance;

    public static EventRouter Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = GameObject.FindObjectOfType<EventRouter>();
            }
            return _Instance;
        }
    }
}
    [Serializable]
    public class ResourceUnityEvent : UnityEvent<ResourceType, bool>
    {
    }

    [Serializable]
    public class DeathUnityEvent : UnityEvent<DeathType>
    {
    }

    [Serializable]
    public class BreakSystemUnityEvent : UnityEvent<ResourceType>
    {
    }

    [Serializable]
    public class GameOverUnityEvent : UnityEvent
    {
    }

    [Serializable]
    public class UnityBoolEvent : UnityEvent<bool>
    {
    }