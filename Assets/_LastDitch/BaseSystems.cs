using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;

public class BaseSystems : MonoBehaviour
{
    private byte
        _power,
        _babies,
        _oxygen,
        _signal;

    public byte MaxBabies = 3;
    public byte MaxPower = 3;
    public byte MaxOxygen = 3;
    public byte MaxSignal = 3;



    public byte Babies
    {
        get { return _babies; }
    }

    public byte Power
    {
        get { return _power; }
    }

    public byte Oxygen
    {
        get { return _oxygen; }
    }

    public byte Signal
    {
        get { return _signal; }
    }




    public static ResourceType[][] ResourcesBrokenOverDays =
    {
        new ResourceType[] {ResourceType.Power},
        new ResourceType[] {ResourceType.Power, ResourceType.Oxygen,},
        new ResourceType[] {ResourceType.Power, ResourceType.Oxygen, ResourceType.Signal},
        new ResourceType[] {ResourceType.Power, ResourceType.Oxygen,},
        new ResourceType[] {ResourceType.Power, ResourceType.Oxygen, ResourceType.Signal},
        new ResourceType[] {ResourceType.Power, ResourceType.Oxygen,},
        new ResourceType[] {ResourceType.Power, ResourceType.Oxygen, ResourceType.Signal},
        new ResourceType[] {ResourceType.Power, ResourceType.Oxygen,},
        new ResourceType[] {ResourceType.Power, ResourceType.Oxygen, ResourceType.Signal}
    };


    private int DayIndex;
    public void AdvanceDay()
    {
        DayIndex++;
        Stamina.Instance.OnNextDay();
        NotifyBrokenSystems();
    }

    void NotifyBrokenSystems()
    {
        var resources = ResourcesBrokenOverDays[DayIndex];
        foreach (var resource in resources)
        {
            // if a system is broken, it subtracts 1 power from that system at the start of the day
            switch (resource)
            {
                case ResourceType.Oxygen:
                    OxygenLoss();
                    break;
                case ResourceType.Power:
                    PowerLoss();
                    break;
                case ResourceType.Signal:
                    SignalLoss();
                    break;
            }

            // it also notifies the system that it requires attention. fixing it will restore the lost power
            // not fixing it will sacrifice that power permanently.
            EventRouter.Instance.BreakSystemEvent.Invoke(resource);
        }
    }

    void Start()
    {
        Initialize();

        // start on day 1
        DayIndex = 0;
        NotifyBrokenSystems();
    }


    public void ResourceHandlerMethod(ResourceType resource, bool isNotPenalty)
    {
        if (isNotPenalty)
        { 
            switch (resource)
            {
                case ResourceType.Oxygen:
                    OxygenGain();
                    break;
                case ResourceType.Power:
                    PowerGain();
                    break;
                case ResourceType.Signal:
                    SignalGain();
                    break;
            }
        }
        else
        {
            switch (resource)
            {
                    case ResourceType.Power:
                    PowerLoss();
                    break;
                    case ResourceType.Signal:
                    SignalLoss();
                    break;
                    case ResourceType.Babies:
                    KillBaby();
                    break;
                    case ResourceType.Oxygen:
                    OxygenLoss();
                    break;
            }
        }
    }



    public void KillBaby(bool allTheBabies = false)
    {
        if (allTheBabies)
        {
            _babies = 0;
            return;
        }

        if (_babies > 0)
            _babies--;
        else
				{
						SoundPlayer.Play("Critical Siren");
            EventRouter.Instance.GameOverEvent.Invoke();
        }
    }



    public void PowerLoss()
    {
        if (_power > 0) _power--;

				SoundPlayer.Play("Warning Siren");

        switch (_power)
        {
            case 2:
                break;
            case 1:
                KillBaby();
                break;
            case 0:
                KillBaby(true);
								SoundPlayer.Play("Critical Siren");
                EventRouter.Instance.GameOverEvent.Invoke();
                break;
        }
    }

    public void PowerGain()
    {
        if (_power < MaxPower)
            _power++;
    }



    public void SignalLoss()
    {
        if (_signal > 0) _signal--;

				SoundPlayer.Play("Warning Siren");

        switch (_signal)
        {
            case 1:
                EventRouter.Instance.RescueStalledEvent.Invoke(true);
                break;
						case 0:
								SoundPlayer.Play("Critical Siren");
                EventRouter.Instance.GameOverEvent.Invoke();
                break;
        }
    }

    public void SignalGain()
    {
        if (_signal < MaxSignal)
            _signal++;

        EventRouter.Instance.RescueStalledEvent.Invoke(false);
    }



    public void OxygenLoss()
    {
        if (_oxygen > 0) _oxygen--;

				SoundPlayer.Play("Warning Siren");

        switch (_oxygen)
        {
            case 2:
                break;
            case 1:
                KillBaby();
                break;
            case 0:
                KillBaby(true);
                EventRouter.Instance.DeathEvent.Invoke(DeathType.Suffocation);
                break;
        }
    }

    public void OxygenGain()
    {
        if (_oxygen < MaxOxygen)
            _oxygen++;
    }



    public void CriticalMissionFailure(DeathType deathType)
    {
				switch(deathType)
				{
					case DeathType.Electrocution:
						SoundPlayer.Play("Electrocution");
						break;
					case DeathType.ExplosiveDecompression:
						SoundPlayer.Play("ExplosiveDecomp1");
						SoundPlayer.Play("DecompressionDeath");
						break;
					case DeathType.OutOfResource:
						SoundPlayer.Play("Critical Siren");
						break;
					case DeathType.Suffocation:
						SoundPlayer.Play("Suffocation");
						break;
					default:
						break;
				}

        EventRouter.Instance.GameOverEvent.Invoke();
    }



    /// <summary>
    ///     Resets all event registers. Sets all resource variables to their predetermined maximums.
    /// </summary>
    public void Initialize()
    {
        _power = MaxPower;
        _babies = MaxBabies;
        _signal = MaxSignal;
        _oxygen = MaxOxygen;
    }

    bool PowerDamaged
    {
        get
        {
            var system =
                GameObject.FindObjectsOfType<BreakableSystem2>()
                    .Where(s => s.ResourceId == ResourceType.Power)
                    .First();
            return system.BrokenToday;
        }
    }

    bool OxygenDamaged
    {
        get
        {
            var system =
                GameObject.FindObjectsOfType<BreakableSystem2>()
                    .Where(s => s.ResourceId == ResourceType.Oxygen)
                    .First();
            return system.BrokenToday;
        }
    }

    bool SignalDamaged
    {
        get
        {
            var system =
                GameObject.FindObjectsOfType<BreakableSystem2>()
                    .Where(s => s.ResourceId == ResourceType.Signal)
                    .First();
            return system.BrokenToday;
        }
    }

    void Update()
    {
			DayHUD.SetValue((DayIndex + 1).ToString());
			StaminaHUD.SetValue(Stamina.Instance.Value.ToString());
			PowerHUD.SetValue(_power.ToString());
			OxygenHUD.SetValue(_oxygen.ToString());
			SignalStrengthHUD.SetValue(_signal.ToString());
		}

		public HUDElement DayHUD;
		public HUDElement StaminaHUD;
		public HUDElement PowerHUD;
		public HUDElement OxygenHUD;
		public HUDElement SignalStrengthHUD;
}