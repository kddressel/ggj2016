using UnityEngine;

public class HydroponicsSequence : MonoBehaviour
{
    public bool AirFilterIsClean = false;
    public bool WaterIsFertilized = false;
    public bool PlantsAreWatered = false;



		public void ToggleAirFilterWorking(bool isWorking)
		{
			if (!isWorking)
			{
				// support breaking things from BaseSystems day script
				AirFilterIsClean = false;
			}
			else
			{
				CleanAirFilter();
			}
		}

		public void ToggleWaterWorking(bool isWorking)
		{
			if (!isWorking)
			{
				// support breaking things from BaseSystems day script
				WaterIsFertilized = false;
			}
			else
			{
				FertilizeTheWater();
			}
		}

		public void TogglePlantWorking(bool isWorking)
		{
			if (!isWorking)
			{
				// support breaking things from BaseSystems day script
				PlantsAreWatered = false;
			}
			else
			{
				WaterThePlants();
			}
		}

    public void WaterThePlants()
    {
        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				PlantsAreWatered = true;
				SoundPlayer.Play("WaterPlants");

        if (!WaterIsFertilized)
				{
						SoundPlayer.Play("Failure");
            EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Oxygen, false);
            return;
        }


        if (!AirFilterIsClean)
				{
						SoundPlayer.Play("Failure");
            EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Oxygen, false);
            return;
        }

				SoundPlayer.Play("Success");
				EventRouter.Instance.ResourceEvent.Invoke(ResourceType.Oxygen, true);
    }




    public void FertilizeTheWater()
    {
        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				WaterIsFertilized = true;
				SoundPlayer.Play("FertilizeWater");
    }


    public void CleanAirFilter()
    {
        if (!Stamina.Instance.CheckAndSubtractStamina()) return;

				AirFilterIsClean = true;
				SoundPlayer.Play("CleanAirVent");
    }
}