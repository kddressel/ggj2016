using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebugInteractablesUI : MonoBehaviour {

	public AirlockSequence Airlock;
	public HydroponicsSequence Hydroponics;
	public RadarSequence Radar;
	public SolarPanelSequence Solar;

	Text Text;

	void Awake()
	{
		Text = GetComponent<Text>();
	}

	void Update()
	{
		Text.text = string.Format("Airlock SuitOn: {0} \n FirstDoorOpen: {1} \n AirlockHasOxygen: {2} \n SecondDoorOpen: {3} \n AirFilterIsClean: {4} \n WaterIsFertilized: {5} \n PlantsAreWatered {6} \n SignalAmplifierIsActive {7} \n DishIsAligned {8} \n ConsoleIsDebugged {9} \n Wings {10} \n Capacitor {11} \n Cord {12}", Airlock.SuitIsOn, Airlock.FirstDoorIsOpen, Airlock.AirlockIsFilledWithAir, Airlock.SecondDoorOpen, Hydroponics.AirFilterIsClean, Hydroponics.WaterIsFertilized, Hydroponics.PlantsAreWatered, Radar.SignalAmplifierIsActive, Radar.DishIsAligned, Radar.ConsoleIsDebugged, Solar.WingsAreWorking, Solar.CapacitorIsWorking, Solar.CordIsWorking);
	}
}
